module.exports = [
    {id: "1", title: "Curso de graphql", views: 1000},
    {id: "2", title: "Curso de Javascript", views: 25000},
    {id: "3", title: "Curso de Programación", views: 100},
    {id: "4", title: "Curso de VueJs", views: 10000},
    {id: "5", title: "Curso de React Native", views: 15000},
    {id: "6", title: "Curso de Laravel", views: 19000},
    {id: "7", title: "Curso de SQL", views: 8000},
]