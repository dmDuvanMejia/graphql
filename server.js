const express = require('express')

const {buildSchema} = require('graphql'); //Define el squema
const graphqlHTTP = require('express-graphql'); 

const app = express();

let courses = require('./courses');

//Schema definition language
const schema = buildSchema(`
    type Course{
       id: ID!,
        title: String!,
        views: Int
    }

    type Alert{
        message : String
    }

    input CourseInput{
        title: String!,
        views: Int
    }

    type Query{
        getCourses: [Course]
        getCourse(id: ID!) : Course
    }

    type Mutation{
        addCourses(input : CourseInput) : Course
        updateCourse(id : ID!, input : CourseInput ) : Course
        deleteCourse(id : ID!) : Alert
    }
`); //template string (Permiten tener cadenas multilinea)
//type Query nos permite declarar las consultas que podemos hacer a nuestro servidor


const root = {
    getCourses(){
        return courses;
    },
    getCourse( { id } ){
        console.log( id );
        return courses.find( (course)=> id == course.id);
    },
    addCourses( { input } ){
        // const { title, views } = input; //Se puede setear los campos asi o 
        // en donde esta  const course = { id, title, views }; cambiar 
        //por  const course = { id, ...input };

        const id = String(courses.length + 1); //Se pone string por que la 
        //firma es string, la firma es como se creo en courses.js

        const course = { id, ...input }; //spread operator
        courses.push(course);
        return course;
    },
    updateCourse( {id, input } ){
        const courseIndex = courses.findIndex( (course)=> id === course.id );
        const course = courses[courseIndex];

        const newCourse = Object.assign( course, input );
        course[courseIndex] = newCourse;

        return newCourse;
    }, 
    deleteCourse( { id } ){
        courses = courses.filter( (course)=> course.id != id );

        return{
            message : `El curso con id ${id} fue eliminado `
        }
    }
}

app.get('/', function(req, res){
    res.json(courses)
})

//middleware, Va a montar el servidor graphql en nuestro servidor,
//le indicamos en donde lo colocara.
app.use('/graphql', graphqlHTTP({
    schema, //Cuando el nombre de una propiedad  y el valor tiene el
    // mismo nombre schema : schema, se puede poner solo el nombre
    rootValue: root,
    graphiql : true //Interfaz grafica
}));

app.listen(8080, function(){
    console.log('Servidor iniciado')
})